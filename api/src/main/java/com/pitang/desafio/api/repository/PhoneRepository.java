package com.pitang.desafio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pitang.desafio.api.model.Phone;

public interface PhoneRepository extends JpaRepository<Phone, Long> {

}
