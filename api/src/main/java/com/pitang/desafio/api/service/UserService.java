package com.pitang.desafio.api.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.pitang.desafio.api.payload.SignInRequest;
import com.pitang.desafio.api.payload.SignUpRequest;

public interface UserService {

	ResponseEntity<?> save(SignUpRequest signUpRequest, HttpServletRequest request);

	ResponseEntity<?> authenticateUser(SignInRequest sigInRequest);

	ResponseEntity<?> getCurrentUser(Long currentUserId);

}
