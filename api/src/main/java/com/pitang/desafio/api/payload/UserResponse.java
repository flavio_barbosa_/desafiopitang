package com.pitang.desafio.api.payload;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pitang.desafio.api.model.Phone;
import com.pitang.desafio.api.model.User;

public class UserResponse {
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private Date createdAt;
	private Date lastLogin;
	private List<PhoneResponse> phones;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private JwtAuthenticationResponse jwt;

	public UserResponse(User user, JwtAuthenticationResponse jwt) {
		loadMainData(user);
		this.jwt = jwt;

	}

	public UserResponse(User user) {
		loadMainData(user);
	}

	private void loadMainData(User user) {
		this.id = user.getId();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.createdAt = user.getCreatedAt();
		this.lastLogin = user.getLastLogin();
		this.phones = new ArrayList<>();
		for (Phone phone : user.getPhones()) {
			this.phones.add(new PhoneResponse(phone));
		}
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<PhoneResponse> getPhones() {
		return phones;
	}

	public void setPhones(List<PhoneResponse> phones) {
		this.phones = phones;
	}

	public JwtAuthenticationResponse getJwt() {
		return jwt;
	}

	public void setJwt(JwtAuthenticationResponse jwt) {
		this.jwt = jwt;
	}

}
