package com.pitang.desafio.api.service;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.pitang.desafio.api.model.Phone;
import com.pitang.desafio.api.model.User;
import com.pitang.desafio.api.payload.ApiErrorResponse;
import com.pitang.desafio.api.payload.JwtAuthenticationResponse;
import com.pitang.desafio.api.payload.SignInRequest;
import com.pitang.desafio.api.payload.PhoneRequest;
import com.pitang.desafio.api.payload.SignUpRequest;
import com.pitang.desafio.api.payload.UserResponse;
import com.pitang.desafio.api.repository.PhoneRepository;
import com.pitang.desafio.api.repository.UserRepository;
import com.pitang.desafio.api.security.JwtTokenProvider;
import com.pitang.desafio.api.service.exception.ResourceNotFoundException;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PhoneRepository phoneRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@Autowired
	AuthenticationManager authenticationManager;

	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public ResponseEntity<?> save(SignUpRequest signUpRequest, HttpServletRequest request) {

		if (signUpRequest.getFirstName() == null || signUpRequest.getFirstName().isEmpty()) {
			return ResponseEntity.badRequest()
					.body(new ApiErrorResponse(400, "Missing fields: Campo 'firstName' obrigatório."));

		} else if (signUpRequest.getLastName() == null || signUpRequest.getLastName().isEmpty()) {
			return ResponseEntity.badRequest()
					.body(new ApiErrorResponse(400, "Missing fields: Campo 'lastName' obrigatório."));

		} else if (signUpRequest.getEmail() == null || signUpRequest.getEmail().isEmpty()) {
			return ResponseEntity.badRequest()
					.body(new ApiErrorResponse(400, "Missing fields: Campo 'email' obrigatório."));

		} else if (signUpRequest.getPassword() == null || signUpRequest.getPassword().isEmpty()) {
			return ResponseEntity.badRequest()
					.body(new ApiErrorResponse(400, "Missing fields: Campo 'password' obrigatório."));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new ApiErrorResponse(400, "E-mail already exists!"));
		}

		for (PhoneRequest phoneRequest : signUpRequest.getPhones()) {
			if (phoneRequest.getAreaCode() == null) {
				return ResponseEntity.badRequest().body(new ApiErrorResponse(400,
						"Missing fields: Campo 'areaCode' obrigatório para todo número de telefone."));

			} else if (phoneRequest.getCountryCode() == null || phoneRequest.getCountryCode().isEmpty()) {
				return ResponseEntity.badRequest().body(new ApiErrorResponse(400,
						"Missing fields: Campo 'countryCode' obrigatório para todo número de telefone."));

			} else if (phoneRequest.getNumber() == null) {
				return ResponseEntity.badRequest().body(new ApiErrorResponse(400,
						"Missing fields: Campo 'number' obrigatório para todo número de telefone."));

			}
		}

		User user = new User();
		user.setFirstName(signUpRequest.getFirstName());
		user.setLastName(signUpRequest.getLastName());
		user.setEmail(signUpRequest.getEmail());
		user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
		user.setCreatedAt(new Date());
		user.setLastLogin(new Date());
		user.setPhones(new ArrayList<>());

		for (PhoneRequest phoneRequest : signUpRequest.getPhones()) {
			Phone phone = new Phone();
			phone.setAreaCode(phoneRequest.getAreaCode());
			phone.setCountryCode(phoneRequest.getCountryCode());
			phone.setNumber(phoneRequest.getNumber());
			phoneRepository.save(phone);
			user.getPhones().add(phone);
		}

		userRepository.save(user);

		String jwt = getJwt(signUpRequest.getEmail(), signUpRequest.getPassword());

		UserResponse userResponse = new UserResponse(user, new JwtAuthenticationResponse(jwt));

		return ResponseEntity.status(HttpStatus.CREATED).body(userResponse);

	}

	public ResponseEntity<?> getCurrentUser(Long currentUserId) {

		User user = userRepository.findById(currentUserId).orElseThrow(
				() -> new ResourceNotFoundException("User not found with username or email : ", "", +currentUserId));

		UserResponse userResponse = new UserResponse(user);
		return ResponseEntity.ok().body(userResponse);
	}

	public ResponseEntity<?> authenticateUser(SignInRequest signInRequest) {

		if (signInRequest.getEmail() == null || signInRequest.getEmail().isEmpty()) {
			return ResponseEntity.badRequest()
					.body(new ApiErrorResponse(400, "Missing fields: Campo 'email' obrigatório."));

		} else if (signInRequest.getPassword() == null || signInRequest.getPassword().isEmpty()) {
			return ResponseEntity.badRequest()
					.body(new ApiErrorResponse(400, "Missing fields: Campo 'password' obrigatório."));
		}

		String jwt = getJwt(signInRequest.getEmail(), signInRequest.getPassword());
		updateLastLogin(signInRequest.getEmail());

		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	}

	private void updateLastLogin(String email) {
		User user = userRepository.findByEmail(email).orElseThrow(
				() -> new ResourceNotFoundException("User not found with username or email : ", "", email));

		user.setLastLogin(new Date());

		userRepository.save(user);

	}

	private String getJwt(String email, String password) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(email, password));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		return tokenProvider.generateToken(authentication);
	}
}
