package com.pitang.desafio.api.payload;

import com.pitang.desafio.api.model.Phone;

public class PhoneResponse {
	
	private Long id;
	
	private Long number;

	private Long areaCode;

	private String countryCode;

	public PhoneResponse(Phone phone) {
		this.setId(phone.getId());
		this.number = phone.getNumber();
		this.areaCode = phone.getAreaCode();
		this.countryCode = phone.getCountryCode();
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public Long getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(Long areaCode) {
		this.areaCode = areaCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
