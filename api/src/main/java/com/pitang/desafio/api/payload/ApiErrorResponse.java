package com.pitang.desafio.api.payload;

public class ApiErrorResponse {
	private int errorCode;
	private String message;

	public ApiErrorResponse(int errorCode, String message) {
		this.setErrorCode(errorCode);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
