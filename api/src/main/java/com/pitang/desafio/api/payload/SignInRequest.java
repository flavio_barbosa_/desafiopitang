package com.pitang.desafio.api.payload;

public class SignInRequest {
	private String email;

	private String password;

	public SignInRequest() {

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
