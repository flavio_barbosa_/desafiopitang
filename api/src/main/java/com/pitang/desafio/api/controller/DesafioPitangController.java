package com.pitang.desafio.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pitang.desafio.api.payload.SignInRequest;
import com.pitang.desafio.api.payload.SignUpRequest;
import com.pitang.desafio.api.repository.UserRepository;
import com.pitang.desafio.api.security.CurrentUser;
import com.pitang.desafio.api.security.UserPrincipal;
import com.pitang.desafio.api.service.UserService;

@RestController
public class DesafioPitangController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	UserService userService;

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@RequestBody SignUpRequest signUpRequest, HttpServletRequest req) {
		return userService.save(signUpRequest, req);
	}

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@RequestBody SignInRequest signInRequest) {
		return userService.authenticateUser(signInRequest);
	}

	@GetMapping("/me")
	public ResponseEntity<?> getCurrentUser(@CurrentUser UserPrincipal currentUser) {
		return userService.getCurrentUser(currentUser.getId());
	}

}
