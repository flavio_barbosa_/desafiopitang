package com.pitang.desafio.api.controller;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pitang.desafio.api.payload.PhoneRequest;
import com.pitang.desafio.api.payload.SignInRequest;
import com.pitang.desafio.api.payload.SignUpRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DesafioPitangControllerTest {

	@Autowired
	public WebApplicationContext context;
	private MockMvc mvc;

	@Before
	public void setup() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void teste01SignupSucesso() throws Exception {
		String url = "/signup";

		SignUpRequest signUpRequest = new SignUpRequest();
		signUpRequest.setEmail("flavio.barbosa.silva@outlook.com");
		signUpRequest.setPassword("teste");
		signUpRequest.setFirstName("Flávio");
		signUpRequest.setLastName("Silva");
		signUpRequest.setPhones(new ArrayList<>());

		PhoneRequest phoneRequest = new PhoneRequest();
		phoneRequest.setAreaCode(Long.valueOf(81));
		phoneRequest.setCountryCode("+55");
		phoneRequest.setNumber(Long.valueOf(99999999));

		signUpRequest.getPhones().add(phoneRequest);

		this.mvc.perform(MockMvcRequestBuilders.post(url).content(asJsonString(signUpRequest))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isCreated())
				.andDo(MockMvcResultHandlers.print());
		;
	}

	@Test
	public void teste02SignupDuplicidadeEmailFalha() throws Exception {
		String url = "/signup";

		SignUpRequest signUpRequest = new SignUpRequest();
		signUpRequest.setEmail("flavio.barbosa.silva@outlook.com");
		signUpRequest.setPassword("teste");
		signUpRequest.setFirstName("Flavio");
		signUpRequest.setLastName("Silva");
		signUpRequest.setPhones(new ArrayList<>());

		PhoneRequest phoneRequest = new PhoneRequest();
		phoneRequest.setAreaCode(Long.valueOf(81));
		phoneRequest.setCountryCode("+55");
		phoneRequest.setNumber(Long.valueOf(99999999));

		signUpRequest.getPhones().add(phoneRequest);

		this.mvc.perform(MockMvcRequestBuilders.post(url).content(asJsonString(signUpRequest))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andDo(MockMvcResultHandlers.print());
		;
	}

	@Test
	public void teste03SignupCamposInvalidosFalha() throws Exception {
		String url = "/signup";

		SignUpRequest signUpRequest = new SignUpRequest();
		signUpRequest.setEmail("");
		signUpRequest.setPassword("teste");
		signUpRequest.setFirstName("");
		signUpRequest.setLastName("Silva");
		signUpRequest.setPhones(new ArrayList<>());

		PhoneRequest phoneRequest = new PhoneRequest();
		phoneRequest.setAreaCode(Long.valueOf(81));
		phoneRequest.setCountryCode("+55");
		phoneRequest.setNumber(Long.valueOf(99999999));

		signUpRequest.getPhones().add(phoneRequest);

		this.mvc.perform(MockMvcRequestBuilders.post(url).content(asJsonString(signUpRequest))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andDo(MockMvcResultHandlers.print());
		;
	}

	@Test
	public void teste04SigninsSucesso() throws Exception {
		String url = "/signin";

		SignInRequest signInRequest = new SignInRequest();
		signInRequest.setEmail("flavio.barbosa.silva@outlook.com");
		signInRequest.setPassword("teste");

		this.mvc.perform(MockMvcRequestBuilders.post(url).content(asJsonString(signInRequest))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print());
		;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
